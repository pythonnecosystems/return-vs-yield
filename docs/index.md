# Return vs Yield: Pyton의 두 가지 결과 경로 <sup>[1](#footnote_1)</sup>

> *Python에서 `return`과 `yield`는 무엇인가? return과 yield를 사용하는 방법 그리고 return과 yield 어느 것이 더 좋은가?*

<a name="footnote_1">1</a>: 이 페이지는 [Return vs. Yield: Python’s Two Pathways to Results](https://medium.com/@HeCanThink/return-vs-yield-pythons-two-pathways-to-results-69354348e17c)를 편역하였다.
